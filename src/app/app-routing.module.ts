import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table/table.component';
import { TableScrollComponent } from './table-scroll/table-scroll.component';

const routes: Routes = [
  { path: '', component: TableComponent },
  { path: 'table-scroll', component: TableScrollComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
