import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class TestServiceService {

  constructor(private http: HttpClient) { }
  // Gets the sample data from a local json file locates in the assets folder
  getData() {
    return this.http.get<any>('../../assets/sample_data.json');
  }

  // Posts the GUID and the status of a user record to API endponint => logs data or error to the console
    sendData(data: object) {
    this.http.post<any>('../', data, httpOptions).subscribe(dataSet => {
      console.log(dataSet);

    }, err => {

      console.log('An error ocurred: ' + err.message);
    });
  }
}
