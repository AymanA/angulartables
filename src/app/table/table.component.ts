import { Component, OnInit } from '@angular/core';
import { TestServiceService } from '../Services/test-service.service';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {


  constructor(private ts: TestServiceService) { }

  noData: boolean; // checks if data was retrevied from the API.
  users: any; // holds raw data retreived from API.
  tableHeaders: any; // tbale headers retrevied from user object keys.
  recordsPerPage = 5; // default number of records per table page
  pagesDropDown = [5, 10, 20]; // a list of reocrds page options for dropdown list
  startingPage = 0; // starting index of records in page
  cutOff = this.recordsPerPage; // ending index of records per page
  pages = []; // Hold the number of pages
  pageSelected = 1; // current page selected in view

  // called from on
  changeNumberOfItemsPerPage(e: any) {
    this.recordsPerPage = e.target.value;
    const pages = [];
    for (let i = 0; i < this.getNumberOfRecords(this.users); i++) {
      pages.push(i + 1);
    }
    this.pages = pages;
    this.cutOff = this.recordsPerPage;
    this.startingPage = 0;
    this.pageSelected = 1;
  }
  // gets the total number of records retreived from API
  getNumberOfRecords(users: any) {
    const numberOfRecords: number = users.length;
    const numberOfPages = numberOfRecords / this.recordsPerPage;
    return numberOfPages;
  }
  // Posts data to the service
  sendData(status: string, guid: string) {
    const data = { status, guid };
    alert(`Status: ${status} \nGUID: ${guid}`);
    this.ts.sendData(data);
  }
  // gets data for the next page
  nextPage() {
    const p = this.pageSelected + 1;
    this.startingPage = (p * this.recordsPerPage) - this.recordsPerPage;
    this.cutOff = p * this.recordsPerPage;
    this.pageSelected = p;
  }
  // get data for the previous page
  previousPage() {
    const p = this.pageSelected - 1;
    this.startingPage = (p * this.recordsPerPage) - this.recordsPerPage;
    this.cutOff = p * this.recordsPerPage;
    this.pageSelected = p;
  }
  // Gets the user data onInit and assigns values
  ngOnInit() {
    this.ts.getData().subscribe(data => {
      this.users = data;
      this.tableHeaders = Object.keys(data[0]);
      this.getNumberOfRecords(this.users);
      const pages = [];
      for (let i = 0; i < this.getNumberOfRecords(this.users); i++) {
        pages.push(i + 1);
      }
      this.pages = pages;
    }, err => {
      this.noData = true;
    });



  }
  // changes the data based on the page number clicked
  changePage(p: number) {
    this.startingPage = (p * this.recordsPerPage) - this.recordsPerPage;
    this.cutOff = p * this.recordsPerPage;
    this.pageSelected = p;
  }

}
