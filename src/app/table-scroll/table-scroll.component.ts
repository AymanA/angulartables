import { Component, OnInit } from '@angular/core';
import { TestServiceService } from '../Services/test-service.service';
@Component({
  selector: 'app-table-scroll',
  templateUrl: './table-scroll.component.html',
  styleUrls: ['./table-scroll.component.css']
})
export class TableScrollComponent implements OnInit {
  recordsPerPage: number;
  users: any;
  tableHeaders: string[];
  pages: any[];
  startingPage: number;
  cutOff: number;
  noData: boolean;
  constructor(private ts: TestServiceService) { }

  // Gets the total number of records retreived from API
  getNumberOfRecords(users: any) {
    const numberOfRecords: number = users.length;
    const numberOfPages = numberOfRecords / this.recordsPerPage;
    return numberOfPages;
  }
  // Loads more records onScroll event
  scrollTable(e: any) {
    this.cutOff = this.cutOff + 1;
  }
  // Posts data to the service using the sendData method in the test service
  sendData(status: string, guid: string) {
    const data = { status, guid };
    alert(`Status: ${status} \nGUID: ${guid}`);
    this.ts.sendData(data);
  }
  // Gets the user data onInit and assigns values
  ngOnInit() {
    this.ts.getData().subscribe(data => {
      this.startingPage = 0;
      this.cutOff = 25;
      this.users = data;
      this.tableHeaders = Object.keys(data[0]);
      this.getNumberOfRecords(this.users);
      const pages = [];
      for (let i = 0; i < this.getNumberOfRecords(this.users); i++) {
        pages.push(i + 1);
      }
      this.pages = pages;
    }, err => {
      this.noData = true;
    });


  }

}
